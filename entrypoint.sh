#!/bin/sh

/etc/init.d/ssh start >/dev/null

echo "$SSHKEY" > /root/.ssh/authorized_keys

if [ "$MESSAGE" ]; then
    echo "$MESSAGE" > /root/README
fi

# Terminate after 1h
sleep ${TIMEOUT:-3600}
